function each(elements, cb) {
    if (typeof elements !== 'object' || typeof cb !== 'function') {
        return;
    }

    for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
    }
}

module.exports = each;