function filter(elements, cb) {
    if (typeof elements != 'object' || typeof cb !== 'function') {
        return [];
    }

    const filterArray = [];

    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            filterArray.push(elements[index]);
        }
    }

    return filterArray;
}

module.exports = filter;