function find(elements, cb) {
    if (typeof elements != 'object' || typeof cb !== 'function') {
        return;
    }

    for (let index = 0; index < elements.length; index++) {
        
        if (cb(elements[index]) === true) {
            return elements[index];
        }
    }

    return;
}

module.exports = find;