const { items } = require('./items.cjs');
const find = require('../find.cjs');

const result = find(items, (item) => {
    return item > 4;
});

console.log(result);