const { items } = require('./items.cjs');
const map = require('../map.cjs');

const cb = (item, index, items) => {
    return item + items[index];
}

const result = map(items, cb);

console.log(result);