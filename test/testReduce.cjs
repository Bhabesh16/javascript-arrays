const { items } = require('./items.cjs');
const reduce = require('../reduce.cjs');

const initialValue = 0;

const cb = (currentValue, value, index, items) => {
    return currentValue + items[index];
}

const result = reduce(items, cb, initialValue);

console.log(result);
